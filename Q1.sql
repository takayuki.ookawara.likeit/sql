create database sql_mondai_shu default character set utf8;
use sql_mondai_shu;
create table item_category (
	category_id int primary key not null auto_increment,
	category_name varchar(256) not null
);